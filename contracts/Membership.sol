//SPDX-License-Identifier: GPL-3.0-only
pragma solidity ^0.6.5;

import "@openzeppelin/contracts/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/SafeERC20.sol";

contract Membership {
  using ECDSA for bytes32;
  using SafeERC20 for IERC20;

  struct Identity {
    uint96 subscriptionExpiration;
    address owner;
    address ownerCandidate;
  }

  /* storage and constants */

  // user address => member info
  mapping(uint => Identity) public identities;
  mapping(address => uint) public memberIds;
  uint public memberCount;

  // used for replay protection
  mapping(bytes32 => bool) public usedSignedMessages;

  address public owner;
  address public ownerCandidate;

  uint public immutable chainId;

  bytes32 constant ENROLL = "ENROLL";
  bytes32 constant SUBSCRIBE = "SUBSCRIBE";

  /* events */

  event Enrolled(uint indexed identity, address indexed owner);
  event Subscribed(uint indexed identity, uint expirationDate);
  event IdentityOffered(uint indexed identity, address indexed from, address indexed to);
  event IdentityTransfered(uint indexed identity, address indexed from, address indexed to);

  /* modifiers */

  modifier onlyOwner {
    require(msg.sender == owner, "Not owner");
    _;
  }

  modifier onlyOwnerCandidate {
    require(msg.sender == ownerCandidate, "Not owner candidate");
    _;
  }

  /* constructor */

  constructor () public {
    uint _chainId;
    assembly {_chainId := chainid()}
    chainId = _chainId;
    owner = msg.sender;
  }

  /* ownership functions */

  function transferOwnership(address candidate) external onlyOwner {
    ownerCandidate = candidate;
  }

  function acceptOwnership() external onlyOwnerCandidate {
    delete ownerCandidate;
    owner = msg.sender;
  }

  /* view functions */

  function getMemberId(address _address) external view returns (uint id) {
    return memberIds[_address];
  }

  function getMemberById(uint id) external view returns (address memberAddress) {
    return identities[id].owner;
  }

  function getMembersByIds(
    uint[] calldata ids
  ) external view returns (
    address[] memory membersAddresses
  ) {

    membersAddresses = new address[](ids.length);

    for (uint i = 0; i < ids.length; i++) {
      membersAddresses[i] = identities[ids[i]].owner;
    }
  }

  function isMember(address _address) external view returns (bool) {
    return memberIds[_address] != 0;
  }

  function isActive(uint id) external view returns (bool) {
    return identities[id].subscriptionExpiration >= block.timestamp;
  }

  function isIdentityOf(uint id, address _address) external view returns (bool) {
    return _address == identities[id].owner;
  }

  function isActiveIdentityOf(uint id, address _address) external view returns (bool) {
    address _owner = identities[id].owner;
    uint expiration = identities[id].subscriptionExpiration;
    return _owner == _address && expiration >= block.timestamp;
  }

  function isValidSignature(
    bytes32 messageDigest,
    bytes memory signature
  ) external view returns (bool) {

    if (usedSignedMessages[messageDigest]) {
      return false;
    }

    // signer check
    bytes32 hash = messageDigest.toEthSignedMessageHash();
    address signer = hash.recover(signature);

    if (signer != owner) {
      return false;
    }

    return true;
  }

  /* state modifying functions */

  // enroll and optionally subscribe
  function enroll(uint period, bytes memory signature) external {

    require(0 == memberIds[msg.sender], "Already enrolled");

    bytes32 messageDigest = keccak256(abi.encode(ENROLL, chainId, msg.sender, period));
    _verifySignature(messageDigest, signature);

    uint subscriptionExpiration = period == 0 ? 0 : (period + block.timestamp);
    require(subscriptionExpiration <= type(uint96).max, "Overflow");

    // indexes start with 1
    uint id = memberCount + 1;
    memberCount = id;

    identities[id].owner = msg.sender;
    identities[id].subscriptionExpiration = uint96(subscriptionExpiration);
    memberIds[msg.sender] = id;

    emit Enrolled(id, msg.sender);

    if (subscriptionExpiration > 0) {
      emit Subscribed(id, subscriptionExpiration);
    }
  }

  // subscribe or extend subscription
  function subscribe(
    uint id,
    uint period,
    uint nonce,
    bytes memory signature
  ) external {

    require(address(0) != identities[id].owner, "Not enrolled");

    bytes32 messageDigest = keccak256(abi.encode(SUBSCRIBE, chainId, id, period, nonce));
    _verifySignature(messageDigest, signature);

    // extend subscription if already has one
    uint startDate = _max(identities[id].subscriptionExpiration, block.timestamp);
    uint newExpiration = startDate + period;
    require(newExpiration <= type(uint96).max, "Overflow");

    identities[id].subscriptionExpiration = uint96(newExpiration);
    emit Subscribed(id, newExpiration);
  }

  function offerIdentityOwnership(address candidate) external {

    uint id = memberIds[msg.sender];

    require(msg.sender == identities[id].owner, "Not identity owner");
    identities[id].ownerCandidate = candidate;

    emit IdentityOffered(id, identities[id].owner, candidate);
  }

  function acceptIdentityOwnership(uint id) external {

    require(msg.sender == identities[id].ownerCandidate, "Not identity owner candidate");

    // change owner and remove candidate
    address previousOwner = identities[id].owner;
    identities[id].owner = msg.sender;
    identities[id].ownerCandidate = address(0);

    // point addresses to correct ids
    delete memberIds[previousOwner];
    memberIds[msg.sender] = id;

    emit IdentityTransfered(id, previousOwner, msg.sender);
  }

  function withdrawTokens(IERC20[] calldata tokens) external onlyOwner {
    for (uint i = 0; i < tokens.length; i++) {
      uint amount = tokens[i].balanceOf(address(this));
      tokens[i].safeTransfer(msg.sender, amount);
    }
  }

  /* internal functions */

  function _verifySignature(bytes32 messageDigest, bytes memory signature) internal {

    // replay protection
    require(usedSignedMessages[messageDigest] == false, "Signature already used");
    usedSignedMessages[messageDigest] = true;

    // signer check
    bytes32 hash = messageDigest.toEthSignedMessageHash();
    address signer = hash.recover(signature);
    require(signer == owner, "Bad signature");
  }

  function _max(uint a, uint b) internal pure returns (uint) {
    return a > b ? a : b;
  }

}
