//SPDX-License-Identifier: GPL-3.0-only
pragma solidity ^0.6.5;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/SafeERC20.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/ReentrancyGuard.sol";
import "./Membership.sol";
import "./ProjectToken.sol";

contract Crowdsale is ReentrancyGuard {
  using Address for address;
  using Address for address payable;
  using SafeERC20 for ERC20;
  using SafeMath for uint;

  enum STATUS {
    CREATED, // initial status
    REFUNDED, // threshold not met, buyers can withdraw their payments
    FINISHED // threshold met, buyers can claim their tokens
  }

  struct Project {
    // slot 0
    uint32 ownerIdentity;
    uint32 saleStart;
    uint32 saleEnd;
    address saleCurrency;
    // slot 1
    uint128 price;
    uint128 minSaleThreshold;
    // slot 2
    uint128 bookedAmount;
    STATUS status;
  }

  struct Booking {
    uint128 amount;
    bool claimed;
  }

  // project token => project details
  mapping(address => Project) public projects;

  // project token => identity => booking
  mapping(address => mapping(uint => Booking)) public bookings;

  // used for replay protection
  mapping(bytes32 => bool) public usedSignedMessages;

  // immutables
  Membership public immutable membership;
  address public immutable investorPool;
  uint public immutable chainId;

  // constants
  uint constant INVESTOR_EQUITY_PERCENTAGE = 10;
  bytes32 constant CREATE = "CREATE";
  address public constant BNB = 0xbBbBBBBbbBBBbbbBbbBbbbbBBbBbbbbBbBbbBBbB;

  // events
  event ProjectCreated(
    address projectToken,
    uint indexed creatorIdentity,
    uint indexed saleStart,
    uint indexed saleEnd
  );

  event TokensBooked(
    address indexed projectToken,
    uint indexed buyerIdentity,
    uint indexed amount
  );

  constructor(address _membership, address _investorPool) public {
    uint _chainId;
    assembly {_chainId := chainid()}
    chainId = _chainId;
    membership = Membership(_membership);
    investorPool = _investorPool;
  }

  function createProject(
    string calldata name,
    string calldata symbol,
    uint supply,
    uint saleStart,
    uint saleEnd,
    address saleCurrency,
    uint price,
    uint minSaleThreshold,
    bytes memory signature
  ) external {

    uint identity = membership.getMemberId(msg.sender);
    require(identity > 0, "Not enrolled");

    // capping max supply to prevent overflows in later calculations, for example during sale
    require(supply <= type(uint128).max, "Total supply exceeds max allowed");

    // creating a project after saleStart date affects duration
    require(saleStart >= block.timestamp, "Sale start cannot be in the past");
    require(saleStart <= type(uint32).max, "Sale start exceeds max allowed");

    require(saleEnd > saleStart, "Sale end must be greater than sale start");
    require(saleEnd <= type(uint32).max, "Sale end exceeds max allowed");

    // needs a max sale duration otherwise funds might get stuck in the contract forever
    require(saleEnd - saleStart <= 180 days, "Sale duration exceeds max allowed");

    if (saleCurrency != BNB) {
      require(saleCurrency.isContract(), "Invalid sale currency token");
    }

    require(price > 0, "Price cannot be zero");
    require(price <= type(uint128).max, "Price exceeds max allowed");

    {
      // overflow check
      (bool ok,) = price.tryMul(supply);
      require(ok, "Price * supply overflows");
    }

    require(minSaleThreshold <= supply, "Minimum sale threshold cannot be greater than supply");

    {
      // signature check
      bytes32 messageDigest = keccak256(
        abi.encode(
          CREATE,
          chainId,
          identity,
          name,
          symbol,
          supply,
          saleStart,
          saleEnd,
          saleCurrency,
          price,
          minSaleThreshold
        )
      );
      require(membership.isValidSignature(messageDigest, signature), "Invalid signature");
      usedSignedMessages[messageDigest] = true;
    }

    // deploy token and mint supply
    address token = address(new ProjectToken(name, symbol, supply));

    // create project
    projects[token] = Project(
      uint32(identity),
      uint32(saleStart),
      uint32(saleEnd),
      saleCurrency,
      uint128(price),
      uint128(minSaleThreshold),
      0,
      STATUS.CREATED
    );

    emit ProjectCreated(token, identity, saleStart, saleEnd);
  }

  function book(
    address projectToken,
    uint amount,
    bool allowPartial
  ) external payable nonReentrant {

    uint memberId = membership.getMemberId(msg.sender);
    require(memberId != 0, "Not enrolled");
    require(membership.isActive(memberId), "Buyer identity has no active subscription");

    Project memory project = projects[projectToken];

    require(project.saleStart <= block.timestamp, "Sale has not yet started");
    require(project.saleEnd > block.timestamp, "Sale has already ended");
    require(project.status == STATUS.CREATED, "Invalid project status");

    uint supply = ERC20(projectToken).totalSupply();
    uint amountOnSale = supply.mul(INVESTOR_EQUITY_PERCENTAGE).div(100);
    uint amountLeft = amountOnSale.sub(project.bookedAmount);

    if (allowPartial) {
      amount = amountLeft;
    } else {
      require(amount <= amountLeft, "Requested amount exceeds amount on sale");
    }

    uint cost = amount.mul(project.price).div(1e18);

    if (project.saleCurrency == BNB) {

      if (msg.value > cost) {
        msg.sender.sendValue(msg.value - cost);
      } else {
        require(msg.value == cost, "Insufficient amount");
      }

    } else {
      require(msg.value == 0, "Call value should be zero");
      ERC20(project.saleCurrency).safeTransferFrom(msg.sender, address(this), cost);
    }

    // update buyer's order
    uint previouslyBooked = bookings[projectToken][memberId].amount;
    bookings[projectToken][memberId].amount = uint128(previouslyBooked.add(amount));

    // update project's booked amount
    projects[projectToken].bookedAmount = uint128(amount.add(project.bookedAmount));

    emit TokensBooked(projectToken, memberId, amount);
  }

  function updateProjectStatus(address projectToken) external {

    Project memory project = projects[projectToken];
    require(project.status != STATUS.CREATED, "Project status already updated");
    require(project.saleEnd < block.timestamp, "Project sale not yet ended");

    bool thresholdMet = project.bookedAmount >= project.minSaleThreshold;
    projects[projectToken].status = thresholdMet ? STATUS.FINISHED : STATUS.REFUNDED;

    if (thresholdMet) {

      // transfer raised amount to project creator
      address creatorAddress = membership.getMemberById(project.ownerIdentity);
      uint soldAmount = project.bookedAmount;
      uint raisedAmount = soldAmount.mul(project.price).div(1e18);
      ERC20(project.saleCurrency).safeTransfer(creatorAddress, raisedAmount);

      // transfer investors' equity
      uint salePercentage = 100 - INVESTOR_EQUITY_PERCENTAGE;
      uint bookedAmount = project.bookedAmount;
      uint investorAmount = bookedAmount.div(salePercentage).mul(INVESTOR_EQUITY_PERCENTAGE);
      ProjectToken(projectToken).transfer(investorPool, investorAmount);

      return;
    }

    ProjectToken(projectToken).burn(project.bookedAmount);
  }

  function claimTokens(address projectToken, uint[] calldata buyerIds) external {

    uint callerId = membership.getMemberId(msg.sender);
    address owner = membership.owner();

    require(
      (buyerIds.length == 1 && buyerIds[0] == callerId) || msg.sender == owner,
      "Caller not authorized"
    );

    STATUS status = projects[projectToken].status;
    require(status != STATUS.CREATED, "Sale has not yet finished");
    require(status != STATUS.REFUNDED, "Sale threshold was not met");

    address[] memory buyers = membership.getMembersByIds(buyerIds);

    for (uint i = 0; i < buyers.length; i++) {

      uint buyerId = buyerIds[i];
      Booking memory booking = bookings[projectToken][buyerId];

      if (booking.claimed) {
        // skipping if already claimed
        continue;
      }

      // mark tokens as claimed
      bookings[projectToken][buyerId].claimed = true;

      // send tokens to buyer
      address buyer = buyers[i];
      ProjectToken(projectToken).transfer(buyer, booking.amount);
    }
  }

  function refund(address projectToken, uint[] calldata buyerIds) external {

    uint callerId = membership.getMemberId(msg.sender);
    address owner = membership.owner();

    require(
      (buyerIds.length == 1 && buyerIds[0] == callerId) || msg.sender == owner,
      "Caller not authorized"
    );

    require(projects[projectToken].status == STATUS.REFUNDED, "Sale is not refunded");
    address[] memory buyers = membership.getMembersByIds(buyerIds);

    ERC20 currency = ERC20(projects[projectToken].saleCurrency);
    uint price = projects[projectToken].price;

    for (uint i = 0; i < buyers.length; i++) {

      uint buyerId = buyerIds[i];
      Booking memory booking = bookings[projectToken][buyerId];

      if (booking.claimed) {
        // skipping if already claimed
        continue;
      }

      // mark refund as claimed
      bookings[projectToken][buyerId].claimed = true;

      // send tokens to buyer
      address buyer = buyers[i];
      uint cost = price.mul(booking.amount).div(1e18);
      currency.safeTransfer(buyer, cost);
    }
  }

}
