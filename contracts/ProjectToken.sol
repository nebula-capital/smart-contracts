//SPDX-License-Identifier: GPL-3.0-only
pragma solidity ^0.6.5;

import "@openzeppelin/contracts/token/ERC20/ERC20Burnable.sol";

contract ProjectToken is ERC20Burnable {

  constructor(
    string memory name_,
    string memory symbol_,
    uint supply
  ) ERC20(name_, symbol_) public {
    _mint(msg.sender, supply);
  }

}
