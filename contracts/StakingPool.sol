//SPDX-License-Identifier: GPL-3.0-only
pragma solidity ^0.6.5;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "./Membership.sol";
import "./Nebula.sol";
import "./ProjectToken.sol";

contract StakingPool {
  using SafeMath for uint;

  // id => shares
  mapping(uint => uint) public balanceOf;

  uint public totalSupply;

  Membership public immutable membership;
  Nebula public immutable nebula;

  event Deposited(uint indexed identity, uint nebulaAmount, uint sharesAmount);
  event Withdrawn(uint indexed identity, uint nebulaAmount, uint sharesAmount);

  constructor (address _membership, address _nebula) public {
    membership = Membership(_membership);
    nebula = Nebula(_nebula);
  }

  function sharesValue(uint sharesAmount) external view returns (uint) {
    uint nebulaBalance = nebula.balanceOf(address(this));
    return sharesAmount.mul(nebulaBalance).div(totalSupply);
  }

  function nebulaValue(uint nebulaAmount) external view returns (uint) {
    uint nebulaBalance = nebula.balanceOf(address(this));
    return nebulaAmount.mul(totalSupply).div(nebulaBalance);
  }

  function deposit(uint depositAmount) external {

    uint memberId = _getActiveMemberId();

    uint sharesTotal = totalSupply;
    uint nebulaBalance = nebula.balanceOf(address(this));
    uint sharesAmount = depositAmount.mul(sharesTotal).div(nebulaBalance);

    nebula.transferFrom(msg.sender, address(this), depositAmount);
    totalSupply = sharesTotal.add(sharesAmount);
    balanceOf[memberId] = balanceOf[memberId].add(sharesAmount);

    emit Deposited(memberId, depositAmount, sharesAmount);
  }

  function withdraw(uint sharesAmount) external {
    uint memberId = _getActiveMemberId();
    _withdraw(memberId, sharesAmount);
  }

  function exit() external {
    uint memberId = _getActiveMemberId();
    _withdraw(memberId, balanceOf[memberId]);
  }

  function _getActiveMemberId() internal view returns (uint memberId) {
    memberId = membership.getMemberId(msg.sender);
    require(memberId != 0, "Not enrolled");
    require(membership.isActive(memberId), "Staker identity has no active subscription");
  }

  function _withdraw(uint memberId, uint sharesAmount) internal {

    uint sharesTotal = totalSupply;
    uint nebulaBalance = nebula.balanceOf(address(this));
    uint withdrawAmount = sharesAmount.mul(nebulaBalance).div(sharesTotal);

    balanceOf[memberId] = balanceOf[memberId].sub(sharesAmount, "Insufficient balnace");
    totalSupply = sharesTotal.sub(sharesAmount);
    nebula.transfer(msg.sender, withdrawAmount);

    emit Withdrawn(memberId, withdrawAmount, sharesAmount);
  }

}
