//SPDX-License-Identifier: GPL-3.0-only
pragma solidity ^0.6.5;

import "./Membership.sol";
import "./Nebula.sol";
import "./ProjectToken.sol";

contract InvestorPool {

  // slot number => identity
  mapping(uint => uint) public ownerIdOf;

  // token => slot => isClaimed
  mapping(address => mapping(uint => bool)) public isClaimed;

  // token => claimedAmount
  mapping(address => uint) public claimedAmounts;

  Membership public immutable membership;
  Nebula public immutable nebula;

  // amount of nebula per slot
  uint public constant SLOT_COST = 500_000 * 1e18;

  // the number of available slots
  uint public constant SLOTS = 40;

  constructor (address _membership, address _nebula) public {
    membership = Membership(_membership);
    nebula = Nebula(_nebula);
  }

  function deposit(uint[] memory slots) external {

    uint id = membership.getMemberId(msg.sender);
    uint slotsUsed;

    for (uint i = 0; i < slots.length; i++) {
      uint slot = slots[i];
      if (ownerIdOf[slot] == 0) {
        ownerIdOf[slot] = id;
        slotsUsed++;
      }
    }

    require(slotsUsed > 0, "No free slots found");

    uint nebulaAmount = slotsUsed * SLOT_COST;
    nebula.transferFrom(msg.sender, address(this), nebulaAmount);
  }

  function withdraw(uint[] memory slots) external {

    uint id = membership.getMemberId(msg.sender);

    for (uint i = 0; i < slots.length; i++) {
      uint slot = slots[i];
      require(ownerIdOf[slot] == id, "Slot not occupied by caller");
      delete ownerIdOf[slot];
    }

    uint nebulaAmount = slots.length * SLOT_COST;
    nebula.transfer(msg.sender, nebulaAmount);
  }

  function claim(address _token, uint[] memory slots) external {

    uint id = membership.getMemberId(msg.sender);
    uint validSlots;

    for (uint i = 0; i < slots.length; i++) {
      uint slot = slots[i];
      require(ownerIdOf[slot] == id, "Slot not occupied by caller");
      require(isClaimed[_token][id] == false, "Slot tokens are already claimed");
      isClaimed[_token][id] = true;
    }

    ProjectToken token = ProjectToken(_token);
    uint currentAmount = token.balanceOf(address(this));
    uint claimedAmount = claimedAmounts[_token];
    uint transferAmount = (claimedAmount + currentAmount) / SLOTS * validSlots;

    claimedAmounts[_token] = claimedAmount + transferAmount;
    token.transfer(msg.sender, transferAmount);
  }

}
