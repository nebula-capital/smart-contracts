//SPDX-License-Identifier: GPL-3.0-only
pragma solidity ^0.6.5;

import "@openzeppelin/contracts/token/ERC20/ERC20Burnable.sol";

contract Nebula is ERC20Burnable {

  constructor(address treasury) ERC20("Nebula", "BULA") public {
    uint totalSupply = 100_000_000 * 1e18;
    _mint(treasury, totalSupply);
  }

}
