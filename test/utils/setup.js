const { ethers } = require('hardhat');

/**
 * @typedef import('../../types').Crowdsale Crowdsale
 * @typedef import('../../types').InvestorPool InvestorPool
 * @typedef import('../../types').Membership Membership
 * @typedef import('../../types').Nebula Nebula
 * @typedef import('../../types').ProjectToken ProjectToken
 * @typedef import('../../types').StakingPool StakingPool
 */

const contracts = {};

module.exports = async function () {

  const [owner] = await ethers.getSigners();

  const Crowdsale = await ethers.getContractFactory('Crowdsale');
  const InvestorPool = await ethers.getContractFactory('InvestorPool');
  const Membership = await ethers.getContractFactory('Membership');
  const Nebula = await ethers.getContractFactory('Nebula');
  const StakingPool = await ethers.getContractFactory('StakingPool');

  /** @type {Membership} */
  const membership = await Membership.deploy();
  await membership.deployed();

  /** @type {Nebula} */
  const nebula = await Nebula.deploy(owner.address);
  await nebula.deployed();

  /** @type {InvestorPool} */
  const investorPool = await InvestorPool.deploy(membership.address, nebula.address);
  await investorPool.deployed();

  /** @type {Crowdsale} */
  const crowdsale = await Crowdsale.deploy(membership.address, investorPool.address);
  await crowdsale.deployed();

  /** @type {StakingPool} */
  const staking = await StakingPool.deploy(membership.address, nebula.address);
  await staking.deployed();

  Object.assign(contracts, { crowdsale, investorPool, membership, nebula, staking });
};

module.exports.contracts = contracts;
