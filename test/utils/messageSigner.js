const { ethers } = require('hardhat');
const {
  defaultAbiCoder: abiCoder,
  arrayify,
  formatBytes32String,
  keccak256,
} = ethers.utils;

const ENROLL = formatBytes32String('ENROLL');
const SUBSCRIBE = formatBytes32String('SUBSCRIBE');
const CREATE = formatBytes32String('CREATE');

const createMessageSigner = wallet => ({

  signEnrollMessage: async (address, period = 0) => {
    const { chainId } = await ethers.provider.getNetwork();
    const message = abiCoder.encode(
      ['bytes32', 'uint256', 'address', 'uint256'],
      [ENROLL, chainId, address, period],
    );
    const messageHash = keccak256(message);
    return wallet.signMessage(arrayify(messageHash));
  },

  signSubscribeMessage: async (identity, period, nonce) => {
    const { chainId } = await ethers.provider.getNetwork();
    const message = abiCoder.encode(
      ['bytes32', 'uint256', 'uint256', 'uint256', 'uint256'],
      [SUBSCRIBE, chainId, identity, period, nonce],
    );
    const messageHash = keccak256(message);
    return wallet.signMessage(messageHash);
  },

  signCreateMessage: async (identity, name, symbol, supply, saleStart, saleDuration, price, minSaleThreshold) => {
    const { chainId } = await ethers.provider.getNetwork();
    const message = abiCoder.encode(
      ['bytes32', 'uint256', 'uint256', 'string', 'string', 'uint256', 'uint256', 'uint256', 'uint256', 'uint256'],
      [CREATE, chainId, identity, name, symbol, supply, saleStart, saleDuration, price, minSaleThreshold],
    );
    const messageHash = keccak256(message);
    return wallet.signMessage(messageHash);
  },

});

module.exports = { createMessageSigner };
