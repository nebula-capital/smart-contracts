const { run } = require('hardhat');

describe('Nebula', function () {

  before(async function () {
    await run('compile', { quiet: true });
  });

  require('./Membership');

});
