const { ethers } = require('hardhat');
const { assert, expect } = require('chai');

const { createMessageSigner } = require('../utils/messageSigner');
const { contracts } = require('../utils/setup');

describe('membership', function () {

  let owner, firstMember, secondMember;
  let membership, messageSigner;

  before(async function () {
    [owner, firstMember, secondMember] = await ethers.getSigners();
    membership = contracts.membership;
    messageSigner = createMessageSigner(owner);
  });

  it('should enroll user', async function () {
    const enrollSignature = await messageSigner.signEnrollMessage(firstMember.address);
    await membership.connect(firstMember).enroll(0, enrollSignature);
    const memberId = await membership.getMemberId(firstMember.address);
    assert.strictEqual(memberId.toString(), '1');
    assert.isTrue(await membership.isMember(firstMember.address));
  });

  it('should increase member count', async function () {
    expect(await membership.memberCount()).to.be.equal('0');
    const enrollSignature = await messageSigner.signEnrollMessage(firstMember.address);
    await membership.connect(firstMember).enroll(0, enrollSignature);
    expect(await membership.memberCount()).to.be.equal('1');
  });

  it('should revert if already enrolled', async function () {

    const enrollSignature = await messageSigner.signEnrollMessage(firstMember.address);
    await membership.connect(firstMember).enroll(0, enrollSignature);

    await expect(
      membership.connect(firstMember).enroll(firstMember.address, enrollSignature),
    ).to.be.revertedWith('Already enrolled');
  });

  it('should revert if when using a bad signature', async function () {

    // sign for a different address
    const badSignature = await messageSigner.signEnrollMessage(secondMember.address);

    await expect(
      membership.connect(firstMember).enroll(firstMember.address, badSignature),
    ).to.be.revertedWith('Bad signature');
  });

  it('should be able to transfer and accept membership', async function () {

    const enrollSignature = await messageSigner.signEnrollMessage(firstMember.address);
    await membership.connect(firstMember).enroll(0, enrollSignature);
    const id = await membership.getMemberId(firstMember.address);

    await membership.connect(firstMember).offerIdentityOwnership(secondMember.address);
    await membership.connect(secondMember).acceptIdentityOwnership(id);

    assert.isFalse(await membership.isMember(firstMember.address));
    assert.isTrue(await membership.isMember(secondMember.address));

    const expectedFirstMemberId = '0';
    const actualFirstMemberid = await membership.getMemberId(firstMember.address);
    assert.strictEqual(actualFirstMemberid.toString(), expectedFirstMemberId);

    const expectedSecondMemberId = id.toString();
    const actualSecondMemberid = await membership.getMemberId(secondMember.address);
    assert.strictEqual(actualSecondMemberid.toString(), expectedSecondMemberId);
  });

  it('should prevent multiple usages of the same signature', async function () {

    const enrollSignature = await messageSigner.signEnrollMessage(firstMember.address);

    await membership.connect(firstMember).enroll(0, enrollSignature);
    const id = await membership.getMemberId(firstMember.address);

    await membership.connect(firstMember).offerIdentityOwnership(secondMember.address);
    await membership.connect(secondMember).acceptIdentityOwnership(id);

    await expect(
      membership.connect(firstMember).enroll(0, enrollSignature),
    ).to.be.revertedWith('Signature already used');
  });

});
