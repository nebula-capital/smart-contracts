const { ethers } = require('hardhat');
const { assert, expect } = require('chai');

const { AddressZero } = ethers.constants;
const { contracts } = require('../utils/setup');

describe('owner', function () {

  let owner, secondOwner, stranger;
  let membership;

  before(async function () {
    [owner, secondOwner, stranger] = await ethers.getSigners();
    membership = contracts.membership;
  });

  it('should set owner at deploy time', async function () {
    assert.strictEqual(await membership.owner(), owner.address);
  });

  it('should be able to change owner', async function () {

    await expect(
      membership.connect(stranger).transferOwnership(secondOwner.address),
    ).to.be.revertedWith('Not owner');

    await expect(
      membership.connect(stranger).acceptOwnership(),
    ).to.be.revertedWith('Not owner candidate');

    await membership.transferOwnership(secondOwner.address);

    await expect(
      membership.connect(stranger).acceptOwnership(),
    ).to.be.revertedWith('Not owner candidate');

    await membership.connect(secondOwner).acceptOwnership();

    await expect(
      membership.connect(stranger).acceptOwnership(),
    ).to.be.revertedWith('Not owner candidate');

    assert.strictEqual(await membership.owner(), secondOwner.address);
    assert.strictEqual(await membership.ownerCandidate(), AddressZero);
  });

});
