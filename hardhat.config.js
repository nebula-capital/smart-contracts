require('@nomiclabs/hardhat-waffle');
require('@nomiclabs/hardhat-ethers');
require('@nomiclabs/hardhat-etherscan');
const { task } = require('hardhat/config');

const env = require('./.env.json');
Object.assign(process.env, env);

task('accounts', 'Prints the list of accounts', async (_, hre) => {
  const accounts = await hre.ethers.getSigners();
  console.log(accounts.map(a => a.address).join('\n'));
});

task('typechain', async (_, { config, run }) => {

  const { tsGenerator } = require('ts-generator');
  const { TypeChain } = require('typechain/dist/TypeChain');

  const cwd = process.cwd();
  const rawConfig = {
    files: `${config.paths.artifacts}/!(build-info|hardhat)/**/+([a-zA-Z0-9]).json`,
    outDir: 'types',
    target: 'ethers-v5',
  };

  await run('compile', { force: true });
  await tsGenerator({ cwd }, new TypeChain({ cwd, rawConfig }));
});

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  etherscan: { apiKey: env.etherscanAPIKey },
  networks: {
    hardhat: {},
    testnet: {
      url: 'https://data-seed-prebsc-1-s1.binance.org:8545',
      chainId: 97,
      gasPrice: 20000000000,
      accounts: [env.privateKey],
    },
    mainnet: {
      url: 'https://bsc-dataseed.binance.org/',
      chainId: 56,
      gasPrice: 20000000000,
      accounts: [env.privateKey],
    },
  },
  solidity: {
    version: '0.6.12',
    settings: { optimizer: { enabled: true, runs: 200 } },
  },
};
