const { ethers, run } = require('hardhat');

async function verify ({ address }, { contract, args: constructorArguments } = {}) {
  await run('verify:verify', { address, contract, constructorArguments });
}

async function main () {

  // compile contracts
  await run('compile', { quiet: true });

  const [owner] = await ethers.getSigners();

  const Crowdsale = await ethers.getContractFactory('Crowdsale');
  const InvestorPool = await ethers.getContractFactory('InvestorPool');
  const Membership = await ethers.getContractFactory('Membership');
  const Nebula = await ethers.getContractFactory('Nebula');
  const StakingPool = await ethers.getContractFactory('StakingPool');

  console.log('Deploying Nebula');
  const nebula = await Nebula.deploy(owner.address);
  await nebula.deployed();

  console.log('Deploying Membership');
  const membership = await Membership.deploy();
  await membership.deployed();

  console.log('Deploying InvestorPool');
  const investorPool = await InvestorPool.deploy(membership.address, nebula.address);
  await investorPool.deployed();

  console.log('Deploying StakingPool');
  const staking = await StakingPool.deploy(membership.address, nebula.address);
  await staking.deployed();

  console.log('Deploying Crowdsale');
  const crowdsale = await Crowdsale.deploy(membership.address, investorPool.address);
  await crowdsale.deployed();

  console.log('Verifying contracts');
  await verify(nebula, { contract: 'contracts/Nebula.sol:Nebula', args: [owner.address] });
  await verify(membership);
  await verify(investorPool, { args: [membership.address, nebula.address] });
  await verify(staking, { args: [membership.address, nebula.address] });
  await verify(crowdsale, { args: [membership.address, investorPool.address] });

  const contracts = { membership, nebula, investorPool, crowdsale, staking };
  const addresses = Object.keys(contracts).reduce((all, name) => {
    return { ...all, [name]: contracts[name].address };
  }, {});

  console.log('Contracts deployed to:', addresses);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
